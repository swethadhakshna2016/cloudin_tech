import { Component, OnInit } from '@angular/core';
import { WeatherServiceService } from '../weather-service.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {
  attributeSelection!: FormGroup;
  city!:string;
  weatherShow:boolean = false;
  weatherData:any;
  forecastData:any;
  showImage:any;
  clearImage:boolean = false;
  fewCloudImage:boolean = false;
  lightRainImage:boolean = false;
  commonImage:boolean = false;
  clearImagefore:boolean = false;
  fewCloudImagefore:boolean = false;
  lightRainImagefore:boolean = false;
  commonImagefore:boolean = false;
  fahernhitValue:any;
  celsiousValue:any;
  imageDescription:any;
  imagefore:any;
  fahernhitValuefore:any;
  celsiousValuefore:any;

  fontStyleControl = new FormControl('');
  fontStyle?: string;

  districtArray =[
    {'id':1, "district": "Erode"},
    {'id':2, "district": "karur"},
    {'id':3, "district": "Ramanathapuram"},
    {'id':4, "district": "Kovai"},
    {'id':5, "district": "Chennai"},
    {'id':6, "district": "Madurai"},
    {'id':7, "district": "Salem"},
    {'id':8, "district": " Tirunelveli"},
    {'id':9,"district":"Nilgiri"}
  ]
  constructor(private weather: WeatherServiceService) { }

  ngOnInit():void {
    // this.city = "swetha";
    this.createForm();
  }

  createForm(){
    this.attributeSelection = new FormGroup({
      pos_code_field: new FormControl('', [Validators.required]),
     
    })
  }

  getWeather(){
    if(!this.attributeSelection.valid) {
      this.attributeSelection.markAllAsTouched(); 
      return;    
    }
      this.weather.weatherGet(this.city).subscribe((data:any) =>{
        // console.log(data);
        this.weatherData =data;
        this.fahernhitValue = (1.8*(this.weatherData.main.temp - 273)+32).toFixed(2);
         this.celsiousValue = ((this.fahernhitValue - 32) * 5/9).toFixed(2);
        //  this.celsiousValue = toFixed(2)
        this.weatherShow = true;
        console.log(data);
        
        this.showImage = data.weather[0].description;
        console.log(this.showImage)

        if(this.showImage == "clear sky"){
          this.clearImage = true;
          this.fewCloudImage = false;
          this.lightRainImage = false;
          this.commonImage = false;
        }
        else if(this.showImage == "few clouds"){
          this.clearImage = false;
          this.fewCloudImage = true;
          this.lightRainImage = false;
          this.commonImage = false;
        }else if(this.showImage == "light rain"){
          this.clearImage = false;
          this.fewCloudImage = false;
          this.lightRainImage = true;
          this.commonImage = false;
        }
        else{
          this.clearImage = false;
          this.fewCloudImage = false;
          this.lightRainImage = false;
          this.commonImage = true;
        }

  

      })

      this.weather.weatherForecast(this.city).subscribe((response:any) =>{
        // console.log(data);
        this.forecastData =response.list;
        this.weatherShow = true;
        console.log(this.forecastData);

      })

    //  for date

   
  }

 
  selectValue(event:any){
    this.city = event.target.value
    console.log(this.city)
  }

  changeSearchFn(event:any){

  }

}
