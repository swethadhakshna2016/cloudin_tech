import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class WeatherServiceService {

  apiKey ='67804d0f60efa3a48e1fbdf1fb344b9a';

  constructor(private http:HttpClient) { }

  weatherGet(city:string){
     return this.http.get('https://api.openweathermap.org/data/2.5/weather?q='+ city + '&appid=' + this.apiKey)
  }

  weatherForecast(city:string){
    return this.http.get('https://api.openweathermap.org/data/2.5/forecast?q='+ city + '&appid=' + this.apiKey + '&cnt=12')
 }
}
